// console.log(`Hello`)

/*Number 3 solution*/
let students = []

function addStudent(name) {
  students.push(name) 
 return(`${name} was added to the student's list`)
}
	//Enter this to console
	// addStudent('John')
	// addStudent('Jane')
	// addStudent('Joe')



/*Number 4 solution*/
function countStudents() {
  console.log(`There are a total of ${students.length} student enrolled.`)
}
	//Enter this to console
	//countStudents()



/*Number 5 solution*/
function printStudent() {
 students.sort(), students.forEach(function(name) {
			console.log(name)
	})
}
	//Enter this to console
	//printStudent()



/*Number 6 solution*/
function findStudent(name) {
  let found = students.filter(function(student) {
    return student.toLowerCase().includes(name.toLowerCase())
  })

  if (found.length === 0) {
    console.log(`${name} is not an enrollee`)
  } else if (found.length === 1) {
    console.log(`${name} is an enrollee`)
  } else if (found.length > 1) {
    console.log(`${found} are enrollees`)
  }
}

	//Enter this to console
	// findStudent('John')
	// findStudent('Jane')
	// findStudent('Joe')
	// findStudent('bill')
	// findStudent('J')
	// findStudent('O')



//----	/*Activity Stretch Goals NOT DONE*/---

// function addSection() {
// students.sort(), students.map(function(name) {
// 			console.log(`${name} - section A`)
// 	})
// }


